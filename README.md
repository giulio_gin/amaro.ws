# 🔗 AMARO LINK

[![](https://img.shields.io/badge/Symfony-5.2-green?style=flat-square&logo=symfony)](https://symfony.com/) 
[![](https://img.shields.io/badge/php-7.4-blue?style=flat-square&logo=php)](https://php.net)

Short and manage links using a stateless API.

## Development

#### Configuration
Start the project with `docker compose up -d` and then use the PHP-FPM container to install the
dependencies using `composer install`.

The application uses the local port `8000`, while the database uses port `8004`.

The database, with fixtures, can be set up by using `composer database-dev-reset`.