<?php

namespace App\Authenticator;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

final class TokenAuthenticator extends AbstractGuardAuthenticator implements AuthenticatorInterface
{
    // Called when authentication is required but not sent.
    public function start(Request $request, AuthenticationException $authException = null): JsonResponse
    {
        $data = [
            'message' => 'Authentication required to perform this action.',
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    // Called to decide if this authenticator should be used.
    public function supports(Request $request): bool
    {
        return $request->headers->has('X-AUTH-TOKEN');
    }

    // Called on every request to get the credentials.
    public function getCredentials(Request $request): ?string
    {
        return $request->headers->get('X-AUTH-TOKEN');
    }

    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        if (null === $credentials) {
            // The header was empty and authentication fails with 401
            return null;
        }

        /*
         * The name of this method is misleading. The "username" here is the attribute "token" as
         * defined in config/packages/security.yaml.
         * If this returns a user, checkCredentials() is called next:
         */
        return $userProvider->loadUserByUsername($credentials);
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        // We don't need to check the credentials when using a token.
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): JsonResponse
    {
        $data = [
            'message' => 'Your token is not valid or has expired.',
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey): void
    {
        // The request continues
    }

    public function supportsRememberMe(): bool
    {
        return false;
    }
}
