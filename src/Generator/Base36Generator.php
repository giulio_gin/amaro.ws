<?php

namespace App\Generator;

use App\Exception\Generator\NonNumericInputException;

final class Base36Generator implements GeneratorInterface
{
    /**
     * Generates a code for the link from its ID.
     *
     * Inspired by reddit: https://github.com/reddit-archive/reddit/blob/753b17407e9a9dca09558526805922de24133d53/r2/r2/lib/utils/_utils.pyx#L28
     *
     * @param string $input The ID of the link
     *
     * @throws NonNumericInputException this method only supports numeric values
     *
     * @return string the generated code
     */
    public function encode(string $input): string
    {
        if (!is_numeric($input)) {
            throw new NonNumericInputException("Value provided to generator is not numeric: {$input}.");
        }

        return base_convert($input, 10, 36);
    }

    /**
     * Given a code, returns the ID of the link.
     */
    public function decode(string $input): string
    {
        return base_convert($input, 36, 10);
    }
}
