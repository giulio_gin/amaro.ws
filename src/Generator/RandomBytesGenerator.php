<?php

namespace App\Generator;

final class RandomBytesGenerator implements GeneratorInterface
{
    /**
     * Generates a random string from 32 random bytes.
     *
     * @param string $input not actually used
     *
     * @throws \Exception this exception is thrown if the app is run on a machine that does not satisfy the requirements
     *                    of the machine functions used to generate the random bytes
     *
     * @return string the random string
     */
    public function encode(string $input): string
    {
        return substr(str_replace('+', '.', base64_encode(random_bytes(32))), 0, 44);
    }

    public function decode(string $input): string
    {
        // No decode available 😇
        return '';
    }
}
