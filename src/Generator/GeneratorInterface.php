<?php

namespace App\Generator;

/*
 * Classes that generate codes should implement this interface.
 * The two methods should be the inverse of each other and they should always produce the same output
 * given the same input.
 */
interface GeneratorInterface
{
    /**
     * Given the input, generates a code.
     */
    public function encode(string $input): string;

    /**
     * Given the input, returns the original value.
     * This method should be the inverse of CodeGeneratorInterface::encode().
     */
    public function decode(string $input): string;
}
