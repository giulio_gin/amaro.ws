<?php

namespace App\Repository;

use App\Entity\LinkInterface;
use App\Entity\ShortLink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ShortLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShortLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShortLink[]    findAll()
 * @method ShortLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class ShortLinkRepository extends ServiceEntityRepository implements LinkRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShortLink::class);
    }

    public function getUserShortLinks(array $params): array
    {
        $query = $this->createQueryBuilder('sl');

        $query
            ->where('sl.author = :author')
            ->setParameter('author', $params['author'])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException|ORMException
     */
    public function persist(LinkInterface $link): void
    {
        $exception = null;
        try {
            $this->_em->persist($link);
            $this->_em->flush();

            return;
        } catch (OptimisticLockException $e) {
            // Not implemented, but if for some reason is thrown, we should know.
            $exception = $e;
        } catch (ORMException $e) {
            $exception = $e;
        }

        throw $exception;
    }

    public function delete(LinkInterface $link): void
    {
        $this->_em->remove($link);
        $this->_em->flush();
    }

    /**
     * @param LinkInterface[] $links
     */
    public function deleteCollection(array $links): void
    {
        foreach ($links as $link) {
            $this->_em->remove($link);
        }

        $this->_em->flush();
    }
}
