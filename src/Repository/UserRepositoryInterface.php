<?php

namespace App\Repository;

use Symfony\Component\Security\Core\User\UserInterface;

interface UserRepositoryInterface
{
    public function persist(UserInterface $user): void;

    public function delete(UserInterface $user): void;
}
