<?php

namespace App\Repository;

use App\Entity\LinkInterface;

interface LinkRepositoryInterface
{
    public function persist(LinkInterface $link): void;

    public function delete(LinkInterface $link): void;
}
