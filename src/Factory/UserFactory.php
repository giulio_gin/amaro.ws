<?php

namespace App\Factory;

use App\Entity\User;
use App\Generator\GeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class UserFactory implements UserFactoryInterface
{
    private UserPasswordEncoderInterface $encoder;
    private GeneratorInterface $generator;

    public function __construct(UserPasswordEncoderInterface $encoder, GeneratorInterface $generator)
    {
        $this->encoder = $encoder;
        $this->generator = $generator;
    }

    public function create(array $params): UserInterface
    {
        $user = new User();
        $user->setEmail($params['email']);
        $user->setPassword(
            $this->encoder->encodePassword($user, $params['password'])
        );
        $user->setToken($this->generator->encode(''));

        return $user;
    }
}
