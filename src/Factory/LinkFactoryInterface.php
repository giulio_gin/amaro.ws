<?php

namespace App\Factory;

use App\Entity\LinkInterface;

interface LinkFactoryInterface
{
    public function create(array $params): LinkInterface;
}
