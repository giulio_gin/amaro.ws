<?php

namespace App\Factory;

use App\Entity\LinkInterface;
use App\Entity\ShortLink;
use App\Exception\Factory\MissingLinkLocationException;

final class LinkFactory implements LinkFactoryInterface
{
    /**
     * @param array{location: string, author: User} $params
     *
     * @throws MissingLinkLocationException
     */
    public function create(array $params): LinkInterface
    {
        if (!isset($params['location'])) {
            throw new MissingLinkLocationException('Missing link location.');
        }

        $link = new ShortLink();
        $link->setLocation($params['location']);
        $link->setAuthor($params['author']);

        return $link;
    }
}
