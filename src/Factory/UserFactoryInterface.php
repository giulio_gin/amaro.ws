<?php

namespace App\Factory;

use Symfony\Component\Security\Core\User\UserInterface;

interface UserFactoryInterface
{
    public function create(array $params): UserInterface;
}
