<?php

namespace App\Controller;

use App\Entity\ShortLink;
use App\Factory\LinkFactory;
use App\Generator\Base36Generator;
use App\Repository\ShortLinkRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/v1/links", name="v1_links_")
 */
class LinksController extends AbstractController
{
    private ?Request $request;

    private LinkFactory $linkFactory;

    private Base36Generator $codeGenerator;

    private ShortLinkRepository $linkRepository;

    public function __construct(
        RequestStack $requestStack,
        LinkFactory $linkFactory,
        Base36Generator $codeGenerator,
        ShortLinkRepository $linkRepository
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->linkFactory = $linkFactory;
        $this->codeGenerator = $codeGenerator;
        $this->linkRepository = $linkRepository;
    }

    /**
     * @Route("", name="create", methods={"POST"})
     * @Route("/", name="create_slash", methods={"POST"})
     */
    public function create(): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $params = [
            'location' => $this->request->request->get('location'),
            'author' => $this->getUser(),
        ];

        $link = $this->linkFactory->create($params);
        $this->linkRepository->persist($link);

        return $this->json([
            'code' => $this->codeGenerator->encode($link->getId()),
        ]);
    }

    /**
     * @Route("/{code}", name="read", methods={"GET"})
     */
    public function read(): RedirectResponse
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_ANONYMOUSLY');

        $code = $this->request->attributes->get('code');

        if (null === $code) {
            return $this->redirect('https://httpstatuses.com/404');
        }

        $link = $this->linkRepository->findOneBy(['id' => $this->codeGenerator->decode($code)]);

        if (null === $link) {
            return $this->redirect('https://httpstatuses.com/404');
        }

        return $this->redirect($link->getLocation());
    }

    /**
     * Returns the list of links created by the user currently in session.
     *
     * @Route("", name="links_list", methods={"GET"})
     * @Route("/", name="links_list_slash", methods={"GET"})
     */
    public function linksList(): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $userLinks = $this->linkRepository->getUserShortLinks(['author' => $this->getUser()]);

        $codeGenerator = $this->codeGenerator;

        return $this->json(array_map(static function (ShortLink $shortLink) use ($codeGenerator) {
            return [
                'code' => $codeGenerator->encode($shortLink->getId()),
                'location' => $shortLink->getLocation(),
                'created_at' => $shortLink->getCreatedAt()->format(DateTime::ATOM),
            ];
        }, $userLinks));
    }
}
