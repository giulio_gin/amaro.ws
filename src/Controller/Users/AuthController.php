<?php

namespace App\Controller\Users;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Has the routes used to authenticate the user.
 * The route structure is "/auth/{method}".
 *
 * @Route("/v1/auth", name="v1_auth_")
 */
final class AuthController extends AbstractController
{
    /**
     * Used to first authenticate the user via email and password to return the user's token.
     * The access to this route is validated by AuthPasswordService.
     *
     * @Route("/password", name="password", methods={"POST"})
     */
    public function password(): JsonResponse
    {
        $this->denyAccessUnlessGranted('IS_ANONYMOUS');

        /** @var User $user */
        $user = $this->getUser();

        return $this->json([
            'token' => $user->getToken(),
        ]);
    }

    /**
     * Used to test that the token is valid.
     * The access to this route is validated by AuthTokenService and the firewall in security.yaml.
     *
     * @Route("/token", name="valid_token", methods={"GET"})
     */
    public function isTokenValid(): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        /** @var User $user */
        $user = $this->getUser();

        return $this->json([
            'email' => $user->getEmail(),
        ]);
    }
}
