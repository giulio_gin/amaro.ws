<?php

namespace App\Controller\Users;

use App\Entity\User;
use App\Factory\UserFactory;
use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Has the routes that deal with registration.
 * - adds a user to the users collection
 * - checks if the username is taken or other fields are valid.
 *
 * @Route("/v1/users", name="v1_users_")
 */
final class RegistrationController extends AbstractController
{
    private ?Request $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @Route("", name="create", methods={"POST"})
     * @Route("/", name="create_slash", methods={"POST"})
     */
    public function create(UserFactory $userFactory, UserRepository $userRepository): JsonResponse
    {
        $this->denyAccessUnlessGranted('IS_ANONYMOUS');

        $params['email'] = $this->request->request->get('email');
        $params['password'] = $this->request->request->get('password');

        /** @var User $user */
        $user = $userFactory->create($params);
        try {
            $userRepository->persist($user);
        } catch (Exception $e) {
            return $this->json(['error' => get_class($e)], Response::HTTP_CONFLICT);
        }

        return $this->json(['token' => $user->getToken()]);
    }
}
