<?php

namespace App\Controller\Users;

use App\Repository\ShortLinkRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Deals with profile related actions.
 * - adds a user to the users collection
 * - checks if the username is taken or other fields are valid.
 *
 * @Route("/v1/users", name="v1_users_")
 */
final class ProfileController extends AbstractController
{
    private ShortLinkRepository $linkRepository;
    private UserRepository $userRepository;

    public function __construct(ShortLinkRepository $linkRepository, UserRepository $userRepository)
    {
        $this->linkRepository = $linkRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Permanently deletes a user profile and everything related.
     * This operation cannot be undone and can only be issued by the user itself.
     *
     * @Route("", name="delete_profile", methods={"DELETE"})
     * @Route("/", name="delete_profile_slash", methods={"DELETE"})
     */
    public function deleteProfile(): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $userLinks = $this->linkRepository->getUserShortLinks(['author' => $this->getUser()]);

        $this->linkRepository->deleteCollection($userLinks);
        $this->userRepository->delete($this->getUser());

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}
