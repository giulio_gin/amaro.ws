<?php

namespace App\Entity;

interface LinkInterface
{
    /**
     * The unique identifier for the link.
     */
    public function getId(): ?int;

    /**
     * @return string|null where the link should go when accessed
     */
    public function getLocation(): ?string;
}
