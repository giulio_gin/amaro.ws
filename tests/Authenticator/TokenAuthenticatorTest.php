<?php

namespace App\Tests\Authenticator;

use App\Authenticator\TokenAuthenticator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * @internal
 * @coversNothing
 */
final class TokenAuthenticatorTest extends TestCase
{
    public function testStart(): void
    {
        $SUT = new TokenAuthenticator();

        $result = $SUT->start(
            $this->createMock(Request::class),
            $this->createMock(AuthenticationException::class)
        );

        $data = [
            'message' => 'Authentication required to perform this action.',
        ];

        $response = new JsonResponse($data, Response::HTTP_UNAUTHORIZED);

        self::assertEquals($response, $result);
    }

    public function testSupports(): void
    {
        $headerBag = $this->createMock(HeaderBag::class);
        $headerBag
            ->expects(self::once())
            ->method('has')
            ->with('X-AUTH-TOKEN')
            ->willReturn(true)
        ;

        $request = $this->createMock(Request::class);
        $request->headers = $headerBag;

        $SUT = new TokenAuthenticator();

        self::assertTrue($SUT->supports($request));
    }

    public function testGetCredentials(): void
    {
        $headerBag = $this->createMock(HeaderBag::class);
        $headerBag
            ->expects(self::once())
            ->method('get')
            ->with('X-AUTH-TOKEN')
            ->willReturn('random-token')
        ;

        $request = $this->createMock(Request::class);
        $request->headers = $headerBag;

        $SUT = new TokenAuthenticator();

        self::assertEquals('random-token', $SUT->getCredentials($request));
    }

    public function testGetUser(): void
    {
        $user = $this->createMock(UserInterface::class);

        $userProvider = $this->createMock(UserProviderInterface::class);
        $userProvider
            ->expects(self::once())
            ->method('loadUserByUsername')
            ->willReturn($user)
        ;

        $SUT = new TokenAuthenticator();

        self::assertEquals($user, $SUT->getUser('credential', $userProvider));
    }

    public function testOnAuthenticationFailure(): void
    {
        $SUT = new TokenAuthenticator();

        $result = $SUT->onAuthenticationFailure(
            $this->createMock(Request::class),
            $this->createMock(AuthenticationException::class)
        );

        self::assertEquals('{"message":"Your token is not valid or has expired."}', $result->getContent());
    }
}
