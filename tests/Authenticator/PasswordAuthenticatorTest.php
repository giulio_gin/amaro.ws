<?php

namespace App\Tests\Authenticator;

use App\Authenticator\PasswordAuthenticator;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * @internal
 * @coversNothing
 */
final class PasswordAuthenticatorTest extends TestCase
{
    public function testSupportsReturnsTrue(): void
    {
        $request = $this->createMock(Request::class);
        $request
            ->expects(self::once())
            ->method('isMethod')
            ->with('POST')
            ->willReturn(true)
        ;

        $parameterBag = $this->createMock(ParameterBag::class);
        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('_route')
            ->willReturn('v1_auth_password')
        ;

        $request->attributes = $parameterBag;

        $SUT = $this->createSUT();
        $result = $SUT->supports($request);

        self::assertTrue($result);
    }

    public function testSupportsReturnsFalseMethodNotPOST(): void
    {
        $request = $this->createMock(Request::class);
        $request
            ->expects(self::once())
            ->method('isMethod')
            ->with('POST')
            ->willReturn(false)
        ;

        $parameterBag = $this->createMock(ParameterBag::class);
        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('_route')
            ->willReturn('v1_auth_password')
        ;

        $request->attributes = $parameterBag;

        $SUT = $this->createSUT();
        $result = $SUT->supports($request);

        self::assertFalse($result);
    }

    public function testSupportsReturnsFalseNotRoute(): void
    {
        $request = $this->createMock(Request::class);
        $request
            ->expects(self::never())
            ->method('isMethod')
            ->with('POST')
            ->willReturn(true)
        ;

        $parameterBag = $this->createMock(ParameterBag::class);
        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('_route')
            ->willReturn('other_route')
        ;

        $request->attributes = $parameterBag;

        $SUT = $this->createSUT();
        $result = $SUT->supports($request);

        self::assertFalse($result);
    }

    public function testGetCredentials(): void
    {
        $request = $this->createMock(Request::class);

        // This is not working!
//        $parameterBag = $this->createMock(ParameterBag::class);
//        $parameterBag
//            ->expects(self::exactly(2))
//            ->method('get')
//            ->willReturnMap([
//                ['email', 'user-email'],
//                ['password', 'user-password']
//            ])
//        ;
//
//        $request->request = $parameterBag;

        // Ugly workaround
        $request->request = new ParameterBag(['email' => 'user-email', 'password' => 'user-password']);

        $SUT = $this->createSUT();
        $result = $SUT->getCredentials($request);

        self::assertEquals('user-email', $result['email']);
        self::assertEquals('user-password', $result['password']);
    }

    public function testGetUser(): void
    {
        $user = $this->createMock(User::class);

        $userRepository = $this->createMock(UserRepository::class);
        $userRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['email' => 'email@example.com'])
            ->willReturn($user)
        ;

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($userRepository)
        ;

        $SUT = $this->createSUT($entityManager);
        $result = $SUT->getUser(['email' => 'email@example.com'], $this->createMock(UserProviderInterface::class));

        self::assertEquals($user, $result);
    }

    public function testGetUserException(): void
    {
        $this->expectException(CustomUserMessageAuthenticationException::class);

        $userRepository = $this->createMock(UserRepository::class);
        $userRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['email' => 'email@example.com'])
            ->willReturn(null)
        ;

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($userRepository)
        ;

        $SUT = $this->createSUT($entityManager);
        $SUT->getUser(['email' => 'email@example.com'], $this->createMock(UserProviderInterface::class));
    }

    public function testCheckCredentials(): void
    {
        $user = $this->createMock(UserInterface::class);

        $passwordEncoder = $this->createMock(UserPasswordEncoderInterface::class);
        $passwordEncoder
            ->expects(self::once())
            ->method('isPasswordValid')
            ->with($user, 'user-password')
            ->willReturn(true)
        ;

        $SUT = $this->createSUT(null, null, $passwordEncoder);

        $result = $SUT->checkCredentials(['password' => 'user-password'], $user);

        self::assertTrue($result);
    }

    public function testGetPassword(): void
    {
        $SUT = $this->createSUT();

        $result = $SUT->getPassword(['password' => 'user-password']);

        self::assertEquals('user-password', $result);
    }

    private function createSUT(
        EntityManagerInterface $entityManager = null,
        UrlGeneratorInterface $urlGenerator = null,
        UserPasswordEncoderInterface $passwordEncoder = null
    ): PasswordAuthenticator {
        if (null === $entityManager) {
            $entityManager = $this->createMock(EntityManagerInterface::class);
        }

        if (null === $urlGenerator) {
            $urlGenerator = $this->createMock(UrlGeneratorInterface::class);
        }

        if (null === $passwordEncoder) {
            $passwordEncoder = $this->createMock(UserPasswordEncoderInterface::class);
        }

        return new PasswordAuthenticator($entityManager, $urlGenerator, $passwordEncoder);
    }
}
